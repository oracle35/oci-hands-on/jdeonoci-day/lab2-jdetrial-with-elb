# lab2-jdetrial-with-elb

Adding a Load-Balancer in front of JD Edwards Trial

## Getting started

The objective of this lab is to secure our JDE Trial instance by adding a Load-Balancer in front of the instance. 
In a production environment, adding a Load-balancer allows you to quickly deploy a highly available JD Edwards instance, easily configure HTTPS and secure any direct access to your JDE instances.

## target architecture

  ![JDE Trial Architecture with elb](./images/jdetrial-elb-architecture.png)

## lab steps

### Task 1. create a load-balancer

Create a load-balancer instance.

1. Sign in to OCI tenancy; from the Oracle CLoud Console home page, click the **Navigation** Menu Button in the upper-left corner and hover over **Networking** and select **Load Balancers**.

    ![elbs-menu](./images/elb-select-menu.png)

2. Make sure you are positioned inside your lab compartment and click **Create Load-Balancer**

    ![elbs-create](./images/elb-select-create.png)

3. Select the Load-Balancer type **Load Balancer**, which is the Layer-7 load-balancer type, and click **Create Load Balancer**

    ![elbs-type](./images/elb-select-type.png)

4. Add Load-Balancer Details; enter the following information and click **Next**:

    * Load Balancer Name: ***lb_xxxx***
    * Visibility Type: ***Public***
    * Public IP address: ***Ephemeral IP Address***
    * Shapes: ***Flexible Shapes***
    * Min Bandwith: ***10 Mbps***
    * MiaxBandwith: ***10-100 Mbps***
    * Virtual Cloud Network: ***VCN created in lab 1***
    * Subnet: ***Public Subnet created in lab 1***

    ![elbs-details](./images/elb-select-details.png)

5. Add Backend information and click **Next**:

    * Load Balancer Policy: ***weighted Round Robin***
    * Click **Add Backend Servers** and select the JDE Trial instance created in Lab 1
    * update the Port Number to ***8079*** instead of 80
    * Health Check Policy
        * Protocol: ***HTTP**
        * Port: ***8079***
        * URL Path: ***/jde/E1Menu.maf***
        * Show Advanced Options:
            * Backend Set Name: ***bs_jde***

    ![elbs-backend](./images/elb-select-backend.png)

6. Add Listener information and click **Next**:

    * Listener Name: ***ls_jde***
    * Type of Traffic: ***HTTP***
    * Port: ***80***

    ![elbs-listener](./images/elb-select-listener.png)

7. Add Logging information and click **Submit**:

    * Error Logs: ***Enabled***
    * Compartment: ***existing Compartment***
    * Auto-Create a default Log Group
        * Log Name: log_lb_jde
        * Log Retention: ***One Month (default)***
    * Access Logs: ***Not Enabled***

    ![elbs-logging](./images/elb-select-logging.png)

### Task 2. update Security Lists

While the load-balancer is being created, Configure Security List:

  * From the details page of the VCN you created in Lab 1, under the Resources section in the left pane, select Security Lists
   * Add an Ingress Rule to allow port 80

      ![sl-port80](./images/sl-port80.png)

   * Update Ingress Rule to ports 8079 and 8080 to close external access and only allow internal communication inside the Subnet

      ![sl-port80](./images/sl-port8079-8080.png)

### Task 3. Allow http connection to the JDE Trial Instance

Connect using SSH to the JDE Trial instance (see Cloud Shell in Lab1) and configure the firewall to open the following ports: 7070, 8079:

```bash
sudo -i
firewall-cmd --permanent --add-port="8079/tcp" 
firewall-cmd --permanent --add-port="7070/tcp" 
firewall-cmd --reload
```

### Task 4. Connect to JD Edwards using the load-balancer

When the Load-Balancer is provisionned you should be able to connect to JD Edwards through it:

* check the Load-Balancer Public IP
* check the load-balancer backend health check which should be **OK**
* connect to JD Edwards using the Load-Balancer IP address: **http://public-load-balancer-IP>/jde** 

  ![elbs-status](./images/elb-select-status.png)


### Optional. Add the Orchestrator Studio to the load-balancer configuration

You can also use Orchestrator Studio with this same load-balancer. Configurations tasks are not detailled here but will involve:

  * adding a new backend set & backend server pointing to the JDE Trial instance, port 7070
  * creating a **Routing Policy** to route JDE and Orchestrator URLs to their proper backend:
    * URL starting with **/jde/** routed to **bs_jde** backend set
    * URL starting with **/studio** or **/jderest** routed to **bs_studio** backend set

    ![elb-routing](./images/elb-routing.png)

  * adding the routing url to the listener created previously
  * updating the Security List to remove external access to port 7070 and allowing only internal CIDR (10.0.1.0/24)


